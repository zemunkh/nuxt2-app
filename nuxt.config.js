export default {
	head: {
		title: 'Nuxt test page',

		htmlAttrs: {
			lang: 'en',
		},

		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: 'Nuxt 2 with tailwindcss.' },
			{ name: 'format-detection', content: 'telephone=no' },
		],
		link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
	},
  server: {
    port: 3000 // default: 3000
  },
  // static: {
  //   prefix: false
  // },
  
	css: ['~/assets/css/main.css'],

	/** auto import components when used in templates */
	components: true,

	buildModules: ['@nuxt/postcss8'],

	modules: [],

	build: {
		/** add tailwind to the postcss configuration */
		postcss: {
			plugins: {
				tailwindcss: {},
				autoprefixer: {},
			},
		},
	},
};
